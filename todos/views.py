from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList
from .forms import TodoListForm
from .forms import TodoItemForm

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, pk=id)
    return render(request, 'todos/todo_detail.html', {'todo_list': todo_list})

def todo_list(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_list.html', {'todo_lists': todo_lists})
def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, pk=id)

    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todos:todo_list_detail', id=id)

    else:
        form = TodoListForm(instance=todo_list)

    return render(request, 'todos/todo_update.html', {'form': form})

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todos:todo_list_detail', id=form.instance.id)
    else:
        form = TodoListForm()

    return render(request, 'todos/todo_create.html', {'form': form})

def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, pk=id)

    if request.method == 'POST':
        todo_list.delete()
        return redirect('todos:todo_list_list')  # Redirect to the list view after deletion

    return render(request, 'todos/todo_delete.html', {'todo_list': todo_list})

def todo_item_create(request, list_id=None):
    todo_list = None

    if list_id is not None:
        todo_list = get_object_or_404(TodoList, pk=list_id)

        if request.method == 'POST':
            form = TodoItemForm(request.POST)
            if form.is_valid():
                # Create a new to-do item associated with the to-do list
                item = form.save(commit=False)
                if todo_list is not None:
                    item.list = todo_list
                item.save()
                return redirect('todos:todo_list_detail', id=list_id)

    else:
        form = TodoItemForm()

    return render(request, 'todos/todo_item_create.html', {'form': form, 'todo_list': todo_list, 'list_id': list_id})

def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, pk=id)

    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect('todos:todo_list_detail', id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    return render(request, 'todos/todo_item_update.html', {'form': form, 'todo_item': todo_item})
